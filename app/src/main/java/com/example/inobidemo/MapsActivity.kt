package com.example.inobidemo

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private lateinit var manager: AssetsManager
    private lateinit var distanceView: TextView
    private var distanceAmount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        distanceView = findViewById(R.id.distance)
        manager = AssetsManager(this)
        initMap()
        showStartDialog()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        val camera = CameraUpdateFactory.newLatLngZoom(LatLng(42.8746, 74.5698), 15f)
        map.moveCamera(camera)
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun showStartDialog() {
        val view = LayoutInflater.from(this)
                .inflate(R.layout.start_dialog, null, false)
        val dialog = AlertDialog.Builder(this)
                .setView(view)
                .setCancelable(false)
                .show()

        view.findViewById<View>(R.id.button).setOnClickListener {
            startTracking()
            dialog.dismiss()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun startTracking() {
        launch(UI) {
            val list = manager.readFile()
            var lastItem: Location = list[0]
            val latLong = LatLng(lastItem.latitude, lastItem.longitude)
            val marker = map.addMarker(MarkerOptions().position(latLong))

            marker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_arrow))
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.position, 18f))

            for (i in 0 until list.size) {
                delay(1000, TimeUnit.MILLISECONDS)
                val item = list[i]
                val cameraPosition = CameraPosition.Builder()
                        .target(LatLng(item.latitude, item.longitude))
                        .zoom(18f)
                if (item.hasBearing()) cameraPosition.bearing(item.bearing)

                marker.isFlat = true
                map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()))
                animateMarker(item, marker)
                distanceAmount += item.distanceTo(lastItem).roundToInt()
                distanceView.text = distanceAmount.toString()
                lastItem = item
            }

            distanceView.text = "Total distance: $distanceAmount"
        }
    }

    private fun animateMarker(destination: Location, marker: Marker) {
        val startPosition = marker.position
        val endPosition = LatLng(destination.latitude, destination.longitude)
        val latLngInterpolator = LatLngInterpolator.LinearFixed()
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = 1000
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { animation ->
            try {
                val v = animation.animatedFraction
                val newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition)
                marker.position = newPosition
                marker.rotation = destination.bearing - 80
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        valueAnimator.start()
    }
}


private interface LatLngInterpolator {
    fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng

    class LinearFixed : LatLngInterpolator {
        override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
            val lat = (b.latitude - a.latitude) * fraction + a.latitude
            var lngDelta = b.longitude - a.longitude
            if (Math.abs(lngDelta) > 180) lngDelta -= Math.signum(lngDelta) * 360
            val lng = lngDelta * fraction + a.longitude
            return LatLng(lat, lng)
        }
    }
}
