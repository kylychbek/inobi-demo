package com.example.inobidemo

import android.content.Context
import android.location.Location
import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader

class AssetsManager(private val context: Context) {
    suspend fun readFile(): MutableList<Location> {
        val locationList = mutableListOf<Location>()
        withContext(DefaultDispatcher) {
            val inputStream = context.assets.open("coords.txt")
            val read = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            read.useLines {
                it.forEach {
                    stringBuilder.append(it)
                }
            }

            try {
                val jsonArray = JSONArray(stringBuilder.toString())
                val size = jsonArray.length()
                for (i in 0 until size) {
                    val jsonObject = jsonArray[i] as JSONObject
                    val location = Location(jsonObject["mProvider"] as String)
                    location.accuracy = (jsonObject["mAccuracy"] as Double).toFloat()
                    location.latitude = jsonObject["mLatitude"] as Double
                    location.longitude = jsonObject["mLongitude"] as Double
                    location.speed = (jsonObject["mSpeed"] as Double).toFloat()
                    location.elapsedRealtimeNanos = jsonObject["mElapsedRealtimeNanos"] as Long
                    location.time = jsonObject["mTime"] as Long
                    location.bearing = (jsonObject["mBearing"] as Double).toFloat()
                    location.altitude = jsonObject["mAltitude"] as Double
                    locationList.add(location)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return locationList
    }
}